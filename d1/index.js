console.log("hello World")



// Assignrmnt operator (=)

let assignmentNumber = 8;

// addition assignemnt operator (+=)


assignmentNumber = assignmentNumber + 2;
console.log(assignmentNumber);//12

assignmentNumber +=2;
console.log(assignmentNumber); //12

// subtraction/multiplication/division assignment operator (-=,*=,/=)

assignmentNumber -=2;
assignmentNumber *=2;
assignmentNumber /=2;

// arithmetic operator (+ - * / %)
let mdas = 1 + 2 - 3 * 4 / 5
console.log("result of mdas operaor" + mdas);

let pemdas = 1 + (2-3)*(4/5);
console.log("result is" + pemdas);

// increment and decrement
let z=1;
//pre-fix incrementarion

++z
console.log(z); //2

//post-fix incrementation
z++
console.log(z);

console.log(z++);
console.log(z);
console.log(++z); //5 the new value os returned immediately

// pre-fix decrementation and post-fix decrementation
console.log(--z);
console.log(z--);

//type Coercion is the automatic  or implicit conversion of values from one data type to another
let numA = '10';
let numB=12;

let coercion = numA+numB;
console.log(typeof coercion);

let numC = 16;
let numD = 14;
let nonCoercion = numC + numD;
console.log(nonCoercion)

let numE = true + 1;
console.log(numE);
console.log(typeof numE);
// the result is  NUMBER
// boolean "true "is associated with the value of 1

let numF = false + 1;
console.log(numF);
console.log(typeof numE);
// the result is  NUMBER
// boolean "false "is associated with the value of 0

// comparison operators
// (==) Equality operator

let juan = 'juan';

console.log("Equality Operator")
console.log(1 == 1);
console.log(1 == 2);
console.log(1 == '1'); //true
console.log(0 == false);
console.log('juan' =='Juan');
console.log('juan' == juan); 

// stricy Equalitu operator
console.log(" Strict Equality Operator")
console.log(1 === 1); //true
console.log(1 === 2);
console.log(1 ==='1');
console.log(0 === false);
console.log('juan' ==='Juan');
console.log('juan' === juan);

// (!=) inequality operator

console.log("inequality Operator")
console.log(1 != 1);
console.log(1 != 2);
console.log(1 != '1'); //true
console.log(0 != false);
console.log('juan' !='Juan');
console.log('juan' != juan); 

// strict Inequality operator
console.log("inequality Operator")
console.log(1 !== 1);
console.log(1 !== 2);
console.log(1 !== '1'); //true
console.log(0 !== false);
console.log('juan' !=='Juan');
console.log('juan' !== juan);

 //relational comparison operator
 let x=500;
 let y=700;
 let w=8000;
 let numString = "5500";

 // Greater that\n (>)
 console.log ("Greater Than")
 console.log (x>y);

 // less than (<)

console.log ("Less Than")
 console.log (y<y);
 console.log(numString < 6000); //forced /type coercion to change string to a numbre
 console.log(numString <1000);

// greater than or equal
console.log ("Greater than or Equal to")
console.log (w >= w);

console.log ("Lessr than or Equal to")
console.log (y <= y); console

// logical operators (&&, ||, !)

let isAdmin = false;
let isRegistered = true;
let isLegalAge=true;

// logical AND operator (&& - double ampersand)
console.log("logical AND Operator")
// return true if all operands are true
let authorization1 = isAdmin && isRegistered;
console.log(authorization1);

let authorization2 = isLegalAge && isRegistered;
console.log(authorization2);

let requiredLevel = 95;
let requiredAge = 18;

let authorization3 = isRegistered && requiredLevel ===25;
console.log(authorization3);

// logical or operato || double pipe
// return true if atleast ONE of the operand are true

let userlevel = 100;
let userlevel2 = 65;

let guildRequirement1 = isRegistered || userlevel2 <=requiredLevel || userAge >= requiredAge;
console.log(guildRequirement1);

// not Operator (!)

console.log ("Not Operator")
// turn a boolean into the opposite value

let opposite =! isAdmin;
console.log(opposite); // true - isAdmin original value is false
console.log(!isRegistered);

let guildAdmin = isAdmin || userlevel2 >= requiredLevel;
console.log(guildAdmin);

// conditional statement
// if else if and if statement

// if statement
// executes a statement if a specified cindition is true

if(true){
	console.log("we just run an if condition!")
}

let numG = 5;
if(numG <10) {
	console.log('Hello');
}

let userName3 = "crusader_1993";
let userLevel3 = 25;
let userAge3 = 20;

if(userName3.length > 10){
	console.log("welcome to Game Online!")
}

if(userName3.length >= 10 && isRegistered && isAdmin){
	console.log("welcome to Game Online!")
} else {
	console.log("you are not ready")
}
// .length is a property of strings which determine the numbe r of characters the the string

// else statement
// execute a statement if all other conditions are false

if (userName3.length >= 10 && userLevel3 >= 25 && userAge3 >=requiredAge){
	console.log("Thank you for joining the Noobies guild")
} else{
	console.log("you are too strong  to be in a nob")
}

//else if statement
//executes a statement if previous conditions are false

if(userName3.length >= 10 && userLevel3 <= 25 && userAge3 >= requiredAge) {
	console.log("Welcome noob")
} else if(userLevel3 > 25) {
	console.log("you are too strong")
} else if (userAge3 < requiredAge) {
	console.log("You are too young to join the guild")
} else if(userName3.length < 10) {
	console.log("Username too short")
} else {
	console.log("you are not ready")
}



//if, else if and else statement with functions

let n = 3;
console.log(typeof n);

function addNum(num1, num2) {
	//check if the numbers being passed as an argument are number types
	if(typeof num1 === "number" && typeof num2 === "number"){
		console.log("Run only if both arguments passed are number types");
		console.log(num1 + num2)
	} else {
		console.log("One or both of the arguments are not numbers")
	}
};

addNum(5, "2");



//create a login function

//check if the username and password is a string type

function login(username, password) {
	if(typeof username === "string" && typeof password === "string") {
		console.log("Both Arguments are string");

		/*
			nested if-else
				will run if the parent if statement is able to agree to accomplish its condition
			Mini Activity

			add another condition to our nested if statement:
				-check if the username and password is atleast 8 characters long.
				-alert - "Thank you fo loggin in"
			add and else statement which will run if both conditions were not met:
				-show an alert message says "Credentials too short"


			Stretch Goals:

			add an else if statement that if the username is less than 8 characters
				-show an alert "Username is too short"
			add an else if statement that if the password is less than 8 characters
				-show an alert "password too shore"

				video/screenshot the ouput

		*/



if(username.length >= 8 && password.length >= 8) {
			console.log("Thank you for logging in")
		} else if(username.length < 8) {
			console.log("username is too short")
		} else if(password.length < 8) {
			console.log("password is too short")
		} else {
			console.log("Credentials too short")
		}


	} else {
	}
};

login("jane", "jane123");
login("j", "jane");


//function with return keyword

let message = 'No message.';
console.log(message)

function determineTyphoonIntensity(windSpeed) {
	if (windSpeed < 30) {
		return 'Not a typhoon yet.';
	} 
	else if (windSpeed <= 61) {
		return 'Tropical depression detected.';
	}
	else if(windSpeed >= 62 && windSpeed <= 88){
		return 'Tropical storm detected.';
	}
	else if (windSpeed >= 89 && windSpeed <= 117) {
		return 'Severe tropical storm detected.';
	} 
	else {
		return 'Typhoon detected.'
	}
}

message = determineTyphoonIntensity(68);
console.log(message); //


if(message == 'Tropical storm detected.'){
	console.warn(message);
}

//console.warn is a good way to print warnings in our console that could help us developers act on a certain output within our code

//Truthy and Falsy
//false(undefined, null, "", NaN, -0)
if (0) {
	console.log("Truthy")
}

let fName = "jane";
let mName = "doe";
let lName = "smith";


console.log(fName + " " + mName + " " + lName)

//Template literals(ES6)

console.log(`${fName} ${mName} ${lName}`)

//Ternary Operator (ES6)
/*
Syntax: 
	(expression/condition) ? ifTrue : ifFalse;
	expression/condition ? ifTrue : ifFalse;



*/
//Single statement execution

let ternaryResult = (1 < 18) ? true : false;
console.log(ternaryResult);


5000 > 1000 ? console.log("price is over 1000") : console.log("Price is less than 1000");



//Else if  with ternary operator

let a = 7;


a === 5
? console.log("A")
: (a === 10 ? console.log("A is 10") : console.log("A is not 5 or 10"))

//Multiple statement execution
let name;

function isOfLegalAge() {
	name = 'John';
	return 'You are of the legal age limit'
}

function isUnderAge() {
	name = 'Jane';
	return 'You are under the age limit'
}

let age = parseInt(prompt("What is your age?"));
console.log(age)

let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
console.log(`Result of the ternary operator in functions: ${legalAge}, ${name}`);

// Switch Statement
/*
	- Can be used as an alternative to an if-else if-else statement.
	- Syntax:
		switch(expression){
			case value1:
				statement;
				break;
			case value2:
				statement;
				break;
			.
			.
			.
			case valuen:
				statement;
				break;
			default:
				statement;
		}
*/

let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day);

switch(day){
	case 'monday':
		console.log("The color of the day is red");
		break;
	case 'tuesday':
		console.log("The color of the day is orange");
		break;
	case 'wednesday':
		console.log("The color of the day is yellow");
		break;
	case 'thursday':
		console.log("The color of the day is green");
		break;
	case 'friday':
		console.log("The color of the day is blue");
		break;
	case 'saturday':
		console.log("The color of the day is indigo");
		break;
	case 'sunday':
		console.log("The color of the day is violet");
		break;
	default:
		console.log("Please input a valid day");
}


// Try-Catch-Finally Statement


function showIntensityAlert(windSpeed){
	try{
		// Attempt to execute a code
		alert(determineTyphoonIntensity(windSpeed));
	}
	catch(error){
		console.log(typeof error);

		console.log(error.message)
	}
	finally{
		// Continue to execute code regardless of success or failure of code execution in the try block
		alert('Intensity updates will show new alert');
	}
}


showIntensityAlert(56);